exports = module.exports = function(server) {
    ///////////////////protocol for messages between cpp nodejs and client//////////
    ///Encode the string:
    ///activ=1 inactiv=0
    ///char0: is cpp program enabled (package box in rutime?)
    ///char1: is the scanner active?
    ///char2: is the start button active?
    ///char3: is the error button active?
    ///char4: is the door opener active?
    /////////////////////////START EASYWSCLIENT ////////////////////////////////////
    var WebSocketServer = require('ws').Server;
    //////////////////////////START SOCKET IO //////////////////////////////////////
    var io = require('socket.io')(server); //socket.io test
    ////////////////////////////SET EASYWSCLIENT MESSAGES///////////////////////////
    var wss = new WebSocketServer({
        server: server,
        path: '/foo'
    });

    wss.on('connection', function(ws) {
        console.log('/foo connected'); //console log if connected

        //initial old_state to only send a message to the state of the box changes
        //this is necessary to provide the running man gif
        var old_state = "connection_lost";
        var webcamstate = "connection_lost";
        var scanbuttonstate = "connection_lost";
        var errorbuttonstate = "connection_lost";
        var dooropenerstate = "connection_lost";

        //send socketio messages if recieving a message from the cpp program
        ws.on('message', function(data, flags) {
            if (flags.binary) {
                return;
            }
            //check if the state of the box changed
            if (old_state != data.charAt(0)) {
                io.sockets.emit('statebox', {
                    description: data.charAt(0)
                });
                old_state = data.charAt(0);
                //console.log(data.charAt(0))
            }
            console.log('>>> ' + data); //consolelog for debugging
            //send states to client if there is a message from cpp
            if (webcamstate != data.charAt(1)) {
                io.sockets.emit('statewebcam', {
                    description: data.charAt(1)
                });
                webcamstate = data.charAt(1);
            }
            if (scanbuttonstate != data.charAt(2)) {
                io.sockets.emit('statescanbutton', {
                    description: data.charAt(2)
                });
                scanbuttonstate = data.charAt(2);
            }
            if (errorbuttonstate != data.charAt(3)) {
                io.sockets.emit('stateerrorbutton', {
                    description: data.charAt(3)
                });
                errorbuttonstate = data.charAt(3);
            }
            if (dooropenerstate != data.charAt(4)) {
                io.sockets.emit('statedooropener', {
                    description: data.charAt(4)
                });
                dooropenerstate = data.charAt(4);
            }
        });

        //send messages to client if the cpp program disconnect
        ws.on('close', function() {
            //console.log('Connection closed!');
            //reset the old state var
            old_state = "connection_lost";
            webcamstate = "connection_lost";
            scanbuttonstate = "connection_lost";
            errorbuttonstate = "connection_lost";
            dooropenerstate = "connection_lost";
            //send to clients that there isn't a connection to cpp anymore
            io.sockets.emit('statebox', {
                description: 'connection_lost'
            });
            io.sockets.emit('statewebcam', {
                description: 'connection_lost'
            });
            io.sockets.emit('statescanbutton', {
                description: 'connection_lost'
            });
            io.sockets.emit('stateerrorbutton', {
                description: 'connection_lost'
            });
            io.sockets.emit('statedooropener', {
                description: 'connection_lost'
            });

        });

        //do something on error
        ws.on('error', function(e) {
            old_state = "connection_lost";
            webcamstate = "connection_lost";
            scanbuttonstate = "connection_lost";
            errorbuttonstate = "connection_lost";
            dooropenerstate = "connection_lost";
            console.log('Error from ws'); //consolelog for debugging
        });

        ////////////////////////////SET SOCKETIO MESSAGES////////////////////////
        io.on('connection', function(socket) {
            //console.log('A user connected'); //consolelog for debugging
            io.sockets.emit('statebox', {
                //old stat must be send by connection because of it's exeption
                //that it's only sended if the state changes
                description: old_state
            });
            io.sockets.emit('statewebcam', {
                description: webcamstate
            });
            io.sockets.emit('statescanbutton', {
                description: scanbuttonstate
            });
            io.sockets.emit('stateerrorbutton', {
                description: errorbuttonstate
            });
            io.sockets.emit('statedooropener', {
                description: dooropenerstate
            });

            //send opendoor data to cpp programm
            socket.on('opendoor', function(data) {
                console.log(data); //console log for debugging
                ws.send('open_door');
            });

            //console output if client disconnect
            socket.on('disconnect', function() {
                //console.log('A user disconnected'); //console log for debugging
            });

        });
        //////////////////////////END SOCKETIO LISTENERS//////////////////////////
    });
    /////////////////////////END EASYWSCLIENT ///////////////////////////////////
}
