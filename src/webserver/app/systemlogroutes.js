module.exports = function(router, db){

  ///////////////////////////// SYSTEMLOG PAGE ///////////////////////////////////
  /**
   * Do the databasequery for the systemlog page and render the page
   * @param {object} res the response object
   * @param {string} order_by_stmt the colums whereby should be sorted
   */
  function select_for_systemlog(order_by_stmt, res) {
      db.serialize(function() {
          var stmt = "SELECT * FROM systemlog ORDER BY "
          if (order_by_stmt == " ") {
              stmt = stmt + "time_stamp DESC";
          } else {
              stmt = stmt + order_by_stmt;
          }
          //array to put the database results in
          var data = [];
          //do the databasequery and render the page
          db.each(stmt,
              function(err, row) {
                  data.push(row)
              },
              function() {
                  res.render('pages/systemlog', {
                      data: data
                  });
              });
      });
  }
  // routes for systemlog page
  router.get('/systemlog', function(req, res) {
      var order_by_stmt = " ";
      select_for_systemlog(order_by_stmt, res);
  });

  router.post('/systemlog', function(req, res) {
      //variable to put the name of the ORDER BY colum in
      var order_by_stmt = " ";
      //check what sorting is wanted and call the databasequery and render function
      if (req.body.eventtype) {
          order_by_stmt = "kind_of_case";
          select_for_systemlog(order_by_stmt, res);
      } else if (req.body.additionalinformation) {
          order_by_stmt = "description";
          select_for_systemlog(order_by_stmt, res);
      } else if (req.body.number) {
          order_by_stmt = "running_number";
          select_for_systemlog(order_by_stmt, res);
      } else {
          select_for_systemlog(order_by_stmt, res);
      }

  });

}
