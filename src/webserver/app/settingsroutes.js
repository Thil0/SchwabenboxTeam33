module.exports = function(router, db, validator){
  ///////////////////////////// SETTINGS PAGE ////////////////////////////////////
  /**
   * Do the databasequery for the settings page and render the page
   * @param {object} res the response object
   */
  function select_for_settings(res) {
      var data = [];
      var runtime = [];
      db.serialize(function() {
          db.each("SELECT * FROM email", function(err, row) {
              data.push(row)
          });
          db.each("SELECT * FROM runtime WHERE rowid = 0", function(err, row) {
              runtime.push(row)
              runtime[0].start = (runtime[0].start + ":00")
              runtime[0].end = ((runtime[0].end - 1) + ":59")
          }, function() {
              res.render('pages/settings', {
                  data: data,
                  runtime: runtime
              });
          });
      });
  }
  /**
   * check if the starttime is earlier than endtime
   * @param {number} starttime start runtime, desired by the user
   * @param {number} endtime end runtime, desired by the user
   * @return {number}   return one if the times are possible
   */
  function check_runtime(starttime, endtime) {
      if (starttime >= endtime) {
          return 0;
      } else {
          return 1;
      }
  }

  // routes for settings page
  router.get('/settings', function(req, res) {
      select_for_settings(res);
  });

  router.post('/settings', function(req, res) {
      //if the button add email was pessed and something is in the input field
      if (req.body.addmail &&
         (req.body.addmailadress != "") &&
         validator.isEmail(req.body.addmailadress)) {
          //console logs for debugging
          //console.log('addmail pressed');
          //console.log('Emailadresse: ' + req.body.addmailadress);
          //console.log('Benutzername: ' + req.body.user);

          db.serialize(function() {
              var user = ""; // safe the input name if its a validate input
              var tryoutuser = req.body.user
              var tryoutuser= tryoutuser.replace(/ /g ,"")
              if ((req.body.user!="") &&
              validator.isAlphanumeric(tryoutuser, ['de-DE'])) {
              user = req.body.user;
            }
              var stmt = db.prepare("INSERT OR IGNORE INTO email VALUES (?,?)");
              stmt.run(user, req.body.addmailadress);
              stmt.finalize;
              select_for_settings(res);
          });
      }
      //if the button remov email was pessed and something is in the input field
      else if (req.body.removemail &&
               validator.isEmail(req.body.removemailadress)) {
          //write number in console
          //console.log('Emailadresse entfernen: ' + req.body.removemailadress);

          db.serialize(function() {
              var stmt = db.prepare("DELETE FROM email WHERE email_adress = (?)");
              stmt.run(req.body.removemailadress);
              stmt.finalize;
              select_for_settings(res);
          });
          //the button set runtime was pressed and the times are plausible
      } else if (req.body.setruntime &&
          check_runtime(req.body.starttime, req.body.endtime)) {
          //console.log('starttime ' + req.body.starttime);
          //console.log('endtime ' + req.body.endtime);
          db.serialize(function() {
              var stmt = db.prepare("UPDATE runtime SET start=(?)," +
                  " end=(?) WHERE rowid=0");
              stmt.run(req.body.starttime, req.body.endtime);
              stmt.finalize;
              select_for_settings(res);
          });
          //default , routing the page without any changes
      } else {
          select_for_settings(res);
      }
  });
}
