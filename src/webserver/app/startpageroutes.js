module.exports = function(router, db){

  ////////////////////////////// START PAGE //////////////////////////////////////
  /**
   * Do the database query an render the overview page
   * @param {object} res the response object
   */
  function select_last_five_delivered(res) {
      var data = []; //init array for database results
      db.serialize(function() {
          db.each("SELECT * FROM Pakete WHERE delivery_time_stamp!= ''" +
              " ORDER BY delivery_time_stamp DESC",
              function(err, row) {
                  data.push(row);
              },
              function() {
                  res.render('pages/home', { //build page
                      data: data //with datas from the database
                  });
              });
      });
  }

  router.get('/', function(req, res) {
      select_last_five_delivered(res);
  });

  router.post('/', function(req, res) {
      select_last_five_delivered(res);
  });


}
