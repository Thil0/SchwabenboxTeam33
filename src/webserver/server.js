//fundamental structure from https://github.com/scotch-io/node-website-course
var express = require('express');
var expressLayouts = require('express-ejs-layouts');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 8080;

// use ejs and express layouts
app.set('view engine', 'ejs');
app.use(expressLayouts);

// use body parser
app.use(bodyParser.urlencoded({
    extended: true
}));

// set static files (css and images, etc) location
app.use(express.static(__dirname + '/public'));

// start the server
const server = app.listen(port, function() {
    console.log('app started');
});

//outsource realtime connection message redirect
var realtime = require('./app/realtimeconnection.js')(server);

// route our app
var router = require('./app/routes');
app.use('/', router);
