//Date for footer
//set Date in accordance with ISO 8601:2004
//return Date in accordance with ISO 8601:2004
var date = new Date();
var year = date.getFullYear();
var month = date.getMonth() + 1;
var day = date.getDate();

month = ((month < 10) ? "0" + month: month);
day = ((day < 10) ? "0" + day : day);

var now = year + "/" + month + "/" + day;
document.getElementById("date").innerHTML = now;
