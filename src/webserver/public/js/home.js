    //packagesbox
    //receives the state of packagesbox,sensor,aktor by server in realtime
    //give on webside back the state of packagesbox,sensor,aktor in figurative sense
    var socket = io();
    //state of packagebox
    socket.on('statebox', function(data) {
      //activ
      if (data.description == 1)
      {
        document.stateofpackagebox.packagebox.src = "images/in_runtime.gif";
      }
      //inactive
      else if (data.description == 0)
      {
        document.stateofpackagebox.packagebox.src = "images/out_of_runtime.jpg";
      }
      //no connection to the websocketclient
      else
      {
        document.stateofpackagebox.packagebox.src = "images/connection_lost.jpg"
      }
    });

    //state of webcam
    socket.on('statewebcam', function(data) {
      //activ
      if (data.description == 1)
      {
        document.stateofsensorandaktor.webcam.src = "../images/aktiv.jpg";
      }
      //inactive
      else
      {
        document.stateofsensorandaktor.webcam.src = "../images/inaktiv.jpg";
      }
    });

    //state of door opener
    socket.on('statedooropener', function(data) {
      //activ
      if (data.description == 1)
      {
        document.stateofsensorandaktor.dooropener.src = "images/aktiv.jpg";
      }
      //inactive
      else
      {
        document.stateofsensorandaktor.dooropener.src = "images/inaktiv.jpg";
      }
    });

    //state of scanbutton
    socket.on('statescanbutton', function(data) {
      //activ
      if (data.description == 1)
      {
        document.stateofsensorandaktor.scanbutton.src = "images/aktiv.jpg";
      }
      //inactive
      else
      {
        document.stateofsensorandaktor.scanbutton.src = "images/inaktiv.jpg";
      }
    });

    //state of errorbutton
    socket.on('stateerrorbutton', function(data) {
      //activ
      if (data.description == 1)
      {
        document.stateofsensorandaktor.errorbutton.src = "images/aktiv.jpg";
      }
      //inactive
      else
      {
        document.stateofsensorandaktor.errorbutton.src = "images/inaktiv.jpg";
      }
    });


    //open the packagebox
    //when it pushed the button "Paketbox öffnen", they send a "1" to the websocketclient
    function opendoor() {
      socket.emit('opendoor', 'open_door');
    }
