#include "ISRs.h"

//=========================these=are=eighty=characters=========================>

using namespace std;


void ISR_start_scan(void)
{
    //! initialisation
    barcode* barcode;
    device* device;

    //! disable scan for "BUTTON_DISABLE_DURATION" seconds
    static int int_disable_time = 0;
    const int BUTTON_DISABLE_DURATION = 5; // seconds

    //! if button not disabled anymore
    if(     device->int_get_seconds_since_19700101()
            >
            ( int_disable_time + BUTTON_DISABLE_DURATION )   )
    {
        //! disable
        int_disable_time = device->int_get_seconds_since_19700101();

        //! if scan not active and device in duty
        if ( !barcode->bol_is_scan_active() && device->bol_device_in_duty() )
        {
            //! start new thread for scan
            new_thread( enums::scan );
        }
    }
}

//=========================these=are=eighty=characters=========================>

void ISR_fail(void)
{
    device* device;

    //! disable fail-thread for "BUTTON_DISABLE_DURATION" seconds
    static int int_disable_time = 0;
    const int BUTTON_DISABLE_DURATION = 5; // seconds

    //! if device in duty
    if ( device->bol_device_in_duty() )
    {
        //! if error button pushed
        if ( digitalRead( enums::ERROR_BUTTON ) == 1 )
        {
            //! set error signal light
            digitalWrite( enums::ERROR_LED, 1 );

            //! if button not disabled anymore
            if ( device->int_get_seconds_since_19700101()
                    > int_disable_time + BUTTON_DISABLE_DURATION )
            {
                //! disable
                int_disable_time = device->int_get_seconds_since_19700101();
                //! send log message to database
                new_thread( enums::fail );
            }
        }
        else
        {
            //! reset error signal light
            digitalWrite( enums::ERROR_LED, 0 );
        }
    }
}
