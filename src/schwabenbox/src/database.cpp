#include <iostream>

#include "database.h"


using namespace std;

//=========================these=are=eighty=characters=========================>

database::database ()
{
    //! nothing to initialize
}

database::~database ()
{
}


void database::vod_insert_log_message( const char& chr_kind_of_case )
{
    //! declare empty string for sql statement
    string str_sqlstatement = "";

    //! switch between kinds of cases, defined at the head of main file
    //! and create statement
    switch (chr_kind_of_case)
    {
    case 1: //! case 1 means no barcode
    {
        str_sqlstatement = "INSERT INTO systemlog " \
                           "(kind_of_case, description, time_stamp)" \
                           "VALUES " \
                           "('Kein Barcode', 'Es wurde kein Barcode erkannt', '"
                           + this->str_get_time_stamp()
                           + "');";
    }
    break;
    case 2: //! case 2 means delivery impossible
    {
        str_sqlstatement = "INSERT INTO systemlog " \
                           "(kind_of_case, description, time_stamp)" \
                           "VALUES " \
                           "('Fehler', 'Der Fehler-Taster wurde betätigt', '"
                           + this->str_get_time_stamp()
                           + "');";
    }
    break;
    default:
        break;
    }

    //! execute statement
    this->vod_change_db_data( str_sqlstatement );
}

//=========================these=are=eighty=characters=========================>

void database::vod_insert_log_message( const char& chr_kind_of_case,
                                       const string& str_current_barcode )
{
    //! declare empty string for sql statement
    string str_sqlstatement = "";

    //! switch between kinds of cases, defined at the head of main file
    //! and create statement
    switch (chr_kind_of_case)
    {
    case 3: //! case 3 means delivered
    {
        str_sqlstatement = "INSERT INTO systemlog " \
                           "(kind_of_case, description, time_stamp)" \
                           "VALUES " \
                           "('Zustellung', 'Paket mit der Nummer " \
                           + str_current_barcode
                           + " wurde zugestellt', '"
                           + this->str_get_time_stamp()
                           + "');";
        break;
    }
    case 4: //! case 4 means unknown barcode
    {
        str_sqlstatement = "INSERT INTO systemlog " \
                           "(kind_of_case, description, time_stamp)" \
                           "VALUES " \
                           "('unbekannter Barcode', 'Barcode mit der Nummer " \
                           + str_current_barcode
                           + " wurde gescannt', '"
                           + this->str_get_time_stamp()
                           + "');";
        break;
    }
    default:
        break;
    }

    //! execute statement
    this->vod_change_db_data( str_sqlstatement );
}

//=========================these=are=eighty=characters=========================>

bool database::bol_is_barcode_in_database( const string& str_current_barcode )
{
    //! declare sqlite3 (-statement) instance
    sqlite3* sql_database;
    sqlite3_stmt* sqs_statement;
    //! declare empty statement-string and integer for number of columns
    string str_sqlstatement = "";
    int int_columns = 0;

    //! opendatabase
    sqlite3_open("../database/schwabenbox.db", &sql_database);

    //! create statement with current barcode
    str_sqlstatement = "SELECT * FROM Pakete "  \
                       "WHERE package_nr='"     \
                     + str_current_barcode
                     + "';";

    // cout << endl << str_sqlstatement << endl << endl;

    //! prepare statement
    sqlite3_prepare( sql_database, str_sqlstatement.c_str(),
                     -1, &sqs_statement, NULL );
    //! get one row of database answer
    sqlite3_step( sqs_statement );
    //! number of columns in data-return of statement
    int_columns = sqlite3_data_count( sqs_statement );
    //! delete prepared statement
    sqlite3_finalize( sqs_statement );
    //! close database
    sqlite3_close( sql_database );
    //cout << int_columns << endl;

    //! if number of columns in data-return more than 0 return true
    return ( int_columns > 0 );
}

//=========================these=are=eighty=characters=========================>

void database::vod_insert_delivery_time_stamp(const string& str_current_barcode)
{
    //! create statement with current time stamp and barcode
    string str_sqlstatement = "UPDATE Pakete " \
                              "SET delivery_time_stamp='" \
                              + this->str_get_time_stamp()
                              + "' " \
                              "WHERE package_nr='"
                              + str_current_barcode
                              + "';";
    //! execute statement
    this->vod_change_db_data( str_sqlstatement );
}


string database::str_get_mail_adresses()
{
    //! declare var. error (unused), create statement
    string str_adresses = "";
    char* chr_error = 0;
    const char* str_sqlstatement = "SELECT email_adress FROM email;";
    //! declare instance of database (-statement)
    sqlite3* sql_database;
    sqlite3_stmt* sqs_statement;
    //! open database
    sqlite3_open("../database/schwabenbox.db", &sql_database);
    //! execute statement with callback function
    sqlite3_exec( sql_database, str_sqlstatement,
                  database::int_callback_adresses,
                  static_cast<void*>(&str_adresses), &chr_error );
    //! close database and return string with all mail adresses
    sqlite3_close( sql_database );
    return str_adresses;
}

//=========================these=are=eighty=characters=========================>


//Source: http://en.cppreference.com/w/cpp/chrono/c/strftime

string database::str_get_time_stamp()
{
    //! get system-time
    time_t tit_time = time(NULL);
    char chr_time[100];
    //! generate time-stamp in format YYYY/MM/DD hh:mm:ss
    if (
        strftime(chr_time, sizeof(chr_time),
        "%Y/%m/%d %T", localtime(&tit_time))
       )
    //! return generated time-stamp
    return chr_time;
}


void database::vod_change_db_data( const string& str_sqlstatement )
{
    //! declare instance of database (-statement)
    sqlite3* sql_database;
    sqlite3_stmt* sqs_statement;
    //! open database
    sqlite3_open("../database/schwabenbox.db", &sql_database);
    //! prepare statement
    sqlite3_prepare( sql_database, str_sqlstatement.c_str(),
                     -1, &sqs_statement, NULL );
    //! get one row of database answer
    sqlite3_step( sqs_statement );
    //! delete prepared statement
    sqlite3_finalize( sqs_statement );
    //! close database
    sqlite3_close( sql_database );
}

//=========================these=are=eighty=characters=========================>


// Source: http://stackoverflow.com/questions/29980607/
//         is-sqlite3-exec-callback-synchronous-or-asynchronous?rq=1

int database::int_callback_adresses(void* scs_adresses, int argc, char** argv,
                                    char** chr_column_name)
{
    string str_adress = "";
    string* str_adresses = static_cast<string*>(scs_adresses);
    for(int i=0; i<argc; i++)
    {
        str_adress += argv[i];
    }
    *str_adresses += str_adress + " ";
    return 0;
}
