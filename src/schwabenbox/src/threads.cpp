#include "threads.h"


//=========================these=are=eighty=characters=========================>

void *vod_open_door(void *i)
{
    //! initialisation
    device* device;
    const int DOOR_OPEN_TIME = 5000000;      // 5 000 000 microseconds

    //! if door opener active, don't set it
    if ( !device->bol_get_state_of_door_opener() )
    {
        //! set door opener
        device->vod_set_door_opener( true );
        //! wait for 5s
        usleep( DOOR_OPEN_TIME );
        //! reset door opener
        device->vod_set_door_opener( false );
    }
    //! exit thread
    return 0;
}

//=========================these=are=eighty=characters=========================>

void *vod_scan(void *i)
{
    //! initialisation
    barcode* barcode;
    database* database;
    device* device;
    email* email;
    string str_current_barcode = "";
    const int SCAN_TIME = 30;                   // 3 seconds
    const int SLEEP_TIME_WHILE_SCAN = 500000;   // 500 000 microseconds

    //! empty barcode-file
    barcode->vod_reset_barcode_file();
    //! start scan
    barcode->vod_scan();
    barcode->vod_scan_active();

    //! scan for "SCAN_TIME" seconds
    int int_start_time = device->int_get_seconds_since_19700101();
    int int_current_time = device->int_get_seconds_since_19700101();

    // Ask for barcode in file
    while ( (   ( int_start_time + SCAN_TIME ) >= int_current_time )
              && !barcode->bol_barcode_in_file()
              &&  barcode->bol_is_scan_active() )
    {
        //! sleep for 500ms to save resources
        usleep( SLEEP_TIME_WHILE_SCAN );
        //! set current time
        int_current_time = device->int_get_seconds_since_19700101();

    }
    //! get barcode and kill zbar
    str_current_barcode = barcode->str_get_barcode();
    barcode->vod_close_scan();
    barcode->vod_scan_inactive();

//=========================these=are=eighty=characters=========================>

    //! if no barcode was detected, send no-barcode-fail-mail
    if ( str_current_barcode == "" )
    {
    //! send fail-mail, log case
        email->vod_send_fail_mail( enums::no_barcode );
        database->vod_insert_log_message( enums::no_barcode );
    }
    //! look for barcode in database
    //! if barcode in database, insert delivery-time-stamp,
    //! send delivery-mail and open door
    else if ( database->bol_is_barcode_in_database( str_current_barcode ))
    {
        //! insert delivery-time-stamp, log case
        database->vod_insert_delivery_time_stamp( str_current_barcode );
        database->vod_insert_log_message( enums::delivered, str_current_barcode );
        //! set door opener
        new_thread( enums::open_door );
        //! send delivery-mail
        email->vod_send_delivery_mail( str_current_barcode );
    }
    //! if barcode not in database, send fail-mail
    else
    {
        //! send fail-mail, log case
        email->vod_send_fail_mail( str_current_barcode );
        database->vod_insert_log_message( enums::unknown_barcode, str_current_barcode );
    }
    //! exit thread
    return 0;
}

//=========================these=are=eighty=characters=========================>

void *vod_fail(void *i)
{
    //! initialisation
    email* email;
    database* database;
    //! send email like "package couldn't be delivered", log case
    email->vod_send_fail_mail( enums::delivery_impossible );
    database->vod_insert_log_message( enums::delivery_impossible );

    //! exit thread
    return 0;
}

//=========================these=are=eighty=characters=========================>

void new_thread( char what_for )
{
    //! initialisation
    pthread_t thread;
    int i = 0;                                         // Needed for new thread

    switch (what_for)
    {
      //! case 1 means open door for 5s
      case enums::open_door:
            pthread_create(&thread, NULL, vod_open_door, (void *)i);
            break;
      //! case 2 means scan barcode
      case enums::scan:
            pthread_create(&thread, NULL, vod_scan, (void *)i);
          break;
      //! case 3 means fail button
      case enums::fail:
          pthread_create(&thread, NULL, vod_fail, (void *)i);
          break;
      default:
          cout << "Es konnte kein neuer Thread gestartet werden!" << endl;
          break;
    }
}
