#ifndef BARCODE_H
#define BARCODE_H

#include <string>
#include <fstream>

using namespace std;

//=========================these=are=eighty=characters=========================>

 /** @class barcode
  * @brief A class to handle barcode scanning
  */


class barcode
{
public:

    /** @brief Constructor
     *
     */
    barcode ();

    /** @brief Destructor
     *
     */
    virtual ~barcode ();


    /** @brief Checks if webcam is active as barcode-scanner
     *
     * @return bool: true if active, false if not active
     *
     */
    bool bol_is_scan_active();


    /** @brief Empties file, where barcode is written inside by scanner
     *
     * @return void
     */
    void vod_reset_barcode_file();

//=========================these=are=eighty=characters=========================>

    /**@brief Starts barcode-scanner
     *
     * @return void
     */
    void vod_scan();


    /** @brief Writes "1" into file "scan_active.txt" for state control
     *
     * @return void
     */
    void vod_scan_active();


    /** @brief Writes "0" into file "scan_active.txt" for state control
     *
     * @return void
     */
     void vod_scan_inactive();


    /** @brief Checks if file "barcode.txt" is empty
     *
     * @return bool: true if barcode in file, false if no barcode in file
     */
    bool bol_barcode_in_file();


    /** @brief Stops barcode-scanner
     *
     * @return void
     */
    void vod_close_scan();


    /** @brief Gets barcode out of "barcode.txt" (open, read, close)
     *
     * @return string: number of scanned barcode
     */
     string str_get_barcode();

//=========================these=are=eighty=characters=========================>

protected:

private:

};
#endif // BARCODE_H
