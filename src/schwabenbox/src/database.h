#ifndef DATABASE_H
#define DATABASE_H

#include <string>
#include <unistd.h>
#include <sqlite3.h>

#include "barcode.h"

using namespace std;

//=========================these=are=eighty=characters=========================>

/**
  * @class database
  * @brief Class for communication with database using sqlite3 commands.
  *
  */

class database
{
public:

    // Constructors/Destructors
    //
    /** @brief Constructor
     *
     */
    database ();

    /** @brief Destructor
     *
     */
    virtual ~database ();

//=========================these=are=eighty=characters=========================>

    /** @brief Inserts different kinds of log messages
     *
     * @param char: kind of log message
     * @return
     */
    void vod_insert_log_message( const char& chr_kind_of_case );


    /** @brief Inserts different kinds of log messages including current barcode
     *
     * @param char: kind of log message
     * @param string: current barcode
     * @return
     */
    void vod_insert_log_message( const char& chr_kind_of_case,
                                 const string& str_current_barcode );


    /** @brief Checks if current barcode is saved in database
     *
     * @param string: current barcode
     * @return true if scanned barcode is in database, false if not
     */
    bool bol_is_barcode_in_database( const string& str_current_barcode );

//=========================these=are=eighty=characters=========================>

    /** @brief Inserts delivery-time-stamp (current system time)
     *
     * @param string: current barcode
     * @return
     */
    void vod_insert_delivery_time_stamp( const string& str_current_barcode );


    /** @brief Generates string with all mail adresses separated by space
     *
     * @return string: all adresses
     */
    string str_get_mail_adresses();

//=========================these=are=eighty=characters=========================>

protected:

private:


    /** @brief Generates time-stamp (current system time)
     *
     * @return string: time-stamp YYYY/MM/DD hh:mm:ss
     */
    string str_get_time_stamp();


    /** @brief Executes database statements without callback
     *
     * @param string: statement to execute
     * @return
     */
    void vod_change_db_data( const string& str_statement );

//=========================these=are=eighty=characters=========================>

    /** @brief Orders all email-adresses of database-table into a chain
     *         separated by one space
     *
     *
     * @param void*: reference to string for adresses
     * @param int: length of char-field argv (from sqlite)
     * @param char** argv: char field for data (from sqlite)
     * @param char** chr_column_name: char field for column name (from sqlite)
     * @return unused
     *
     * Source: http://stackoverflow.com/questions/29980607/
     * is-sqlite3-exec-callback-synchronous-or-asynchronous?rq=1
     */
    static int int_callback_adresses(void* scs_adresses, int argc, char** argv,
                                     char** chr_column_name);

};
#endif // DATABASE_H
