#include "email.h"

//=========================these=are=eighty=characters=========================>

email::email ()
{
    //! nothing to initialize
}

email::~email ()
{
}

//"Mit 'sudo chfn -f ""Schwabenbox"" <altueller " \
//"Benutzername>' kann der angezeigte Absender geaendert werden!" \


void email::vod_send_fail_mail( const char& kind_of_case )
{
    //! declare database instance and empty command string
    database* database;
    string str_command = "";
    string str_adresses = database->str_get_mail_adresses();
    // cout << str_adresses << endl; // just fpr testing

    //! if mail adresses in database
    if ( !str_adresses.empty() )
    {
        //! define command according to kind of case
        //! and prepare for sending by command line
        switch (kind_of_case)
        {
        case enums::no_barcode: //! case 1 means no barcode
        {
            str_command = "echo An Ihrer Schwabenbox wurde beim " \
                          "Scanvorgang kein Barcode erkannt. " \
                          " | mail -s \" kein Barcode erkannt \" ";
        }
        break;
        case enums::delivery_impossible: //! case 2 means delivery impossible
        {
            str_command = "echo An Ihrer Schwabenbox wurde die " \
                          "Fehler-Taste betaetigt. " \
                          " | mail -s \" Fehler-Taste betaetigt \" ";
        }
        break;
        default:
            break;

        }
        str_command += str_adresses;

        //! execute command
        system(str_command.c_str());
    }
    else // just for testing
    {
        //cout << "keine Mailadressen eingetragen" << endl;
    }
}

//=========================these=are=eighty=characters=========================>

void email::vod_send_fail_mail( const string& str_current_barcode )
{
    //! declare database instance
    database* database;

    string str_adresses = database->str_get_mail_adresses();
    // cout << str_adresses << endl; // just fpr testing

    //! if mail adresses in database
    if ( !str_adresses.empty() )
    {
        //! define command according to kind of case
        //! and prepare for sending by command line
        string str_command =
            "echo An Ihrer Schwabenbox wurde ein unerwartetes Paket mit der Nummer "
            + str_current_barcode
            + " erkannt. " \
            " | mail -s \" unerwartetes Paket \" "
            + str_adresses;

        //! execute command
        system(str_command.c_str());
    }
    else // just for testing
    {
        //cout << "keine Mailadressen eingetragen" << endl;
    }
}



void email::vod_send_delivery_mail( const string& str_current_barcode )
{
    //! declare database instance
    database* database;

    string str_adresses = database->str_get_mail_adresses();
    // cout << str_adresses << endl; // just fpr testing

    //! if mail adresses in database
    if ( !str_adresses.empty() )
    {
        //! define command according to kind of case
        //! and prepare for sending by command line
        string str_command =
            "echo An Ihrer Schwabenbox wurde das erwartete Paket mit der Nummer "
            + str_current_barcode
            + " zugestellt. " \
            "| mail -s \" Paket zugestellt \" "
            + str_adresses;

        //! execute command
        system(str_command.c_str());
    }
    else // just for testing
    {
        //cout << "keine Mailadressen eingetragen" << endl;
    }
}

//=========================these=are=eighty=characters=========================>
