#include "barcode.h"


//=========================these=are=eighty=characters=========================>

barcode::barcode ()
{
    //! nothing to initialize
}

barcode::~barcode ()
{
}


bool barcode::bol_is_scan_active()
{
    //! declare filestream and string for first line of file
    ifstream ifs_filestream;
    string str_firstline = "";
    //! open filestream to scan_active.txt
    ifs_filestream.open( "scan_active.txt", ios::in);
    //! get first line of file
    getline(ifs_filestream, str_firstline);
    //! if first line in file is just "1" close filestream and return true
    if ( str_firstline == "1" )
    {
        ifs_filestream.close();
        return true;
    //! else close filestream and return false
    }
    else
    {
        ifs_filestream.close();
        return false;
    }
}

//=========================these=are=eighty=characters=========================>

void barcode::vod_reset_barcode_file()
{
    //! declare bash command for emtptying file barcode.txt and execute
    string str_empty_file = "echo """" > barcode.txt";
    system(str_empty_file.c_str());
}


void barcode::vod_scan()
{
    //! declare bash command for starting zbarcam and execute
    string str_start_zbar = "zbarcam --raw --nodisplay > barcode.txt &";
    system(str_start_zbar.c_str());
}


void barcode::vod_scan_active()
{
    //! declare bash command for writing "1" into file scan_active.txt and exec.
    string str_scan_active = "echo ""1"" > scan_active.txt";
    system(str_scan_active.c_str());
}


void barcode::vod_scan_inactive()
{
    //! declare bash command for writing "0" into file scan_active.txt and exec.
    string str_scan_inactive = "echo ""0"" > scan_active.txt";
    system(str_scan_inactive.c_str());
}

//=========================these=are=eighty=characters=========================>

bool barcode::bol_barcode_in_file()
{
    //! declare filestream and string for first line of file
    ifstream ifs_filestream;
    string str_firstline = "";

    //! open filestream to barcode.txt
    ifs_filestream.open( "barcode.txt", ios::in);
    //! get first line of file
    getline(ifs_filestream, str_firstline);
    //! close filestream
    ifs_filestream.close();

    //! return true if first line of file not empty, false if not
    return !(str_firstline == "");
}


void barcode::vod_close_scan()
{
    //! declare bash command for closing zbarcam and execute
    string str_kill_zbar = "killall zbarcam";
    system(str_kill_zbar.c_str());
}


string barcode::str_get_barcode()
{
    //! declare filestream and string for first line of file
    ifstream ifs_filestream;
    string str_firstline = "";

    //! open filestream to barcode.txt
    ifs_filestream.open( "barcode.txt", ios::in);
    //! get first line of file
    getline(ifs_filestream, str_firstline);
    //! close filestream
    ifs_filestream.close();

    //! return barcode in string
    return str_firstline;
}

//=========================these=are=eighty=characters=========================>
